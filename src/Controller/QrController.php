<?php

namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Swift_Mailer;
use Swift_Message;

/**
 * @Route("/qr")
 */
class QrController extends AbstractController
{

    /**
     * @Route("/read", name="qr_read", methods={"POST"})
     */
    public function index()
    {
        $request = $this->get('request_stack')->getCurrentRequest();
        $data = json_decode($request->getContent(), true);
        $respuesta = [];
        $codigo = $data['codigo'] ?? null;

        if ($codigo == null) {
            $respuesta = ['status' => '0', 'message' => 'Falta Id o Código'];
            return new  JsonResponse($respuesta);
        }

        $em = $this->getDoctrine()->getManager();
        $qr = $em->getRepository('App:Qr')->findOneBy(array('codigo' => $codigo));

        if ($qr) {
            $descuento = $qr->getDescuento();
            if ($qr->getUsed() == false || $qr->getUsed() == null) {
                $message = ['titulo' => $descuento->getTitulo(), 'porcentaje' => $descuento->getPorcentaje(), 'cliente' => $qr->getClient()->getName()];
                $respuesta = ['status' => '1', 'message' => $message];
                $qr->setUsed(true);
                $em->persist($qr);
                $em->flush();
            } else {
                $message = ['titulo' => $descuento->getTitulo(), 'porcentaje' => $descuento->getPorcentaje(), 'cliente' => $qr->getClient()->getName()];
                $respuesta = ['status' => '0', 'message' => $message];
            }
        } else {
            $respuesta = ['status' => '0', 'message' => 'Código Incorrecto'];
        }


        return new JsonResponse($respuesta);
    }


    /**
     * @Route("/mensaje", name="mensaje", methods={"POST"})
     */
    public function mensaje()
    {
        $request = $this->get('request_stack')->getCurrentRequest();
        $data = json_decode($request->getContent(), true);
        $respuesta = [];
        $mensaje = $data['mensaje'] ?? null;
        $clienteId = $data['cliente'] ?? null;


        $em = $this->getDoctrine()->getManager();

        $cliente = $em->getRepository('App:Client')->find($clienteId);

        $transport = (new \Swift_SmtpTransport('mail.bodegalostoneles.com', 465, "ssl"))
            ->setUsername('turismo@bodegalostoneles.com')
            ->setPassword('1922Toneles');

        $mailer = new \Swift_Mailer($transport);
        $logger = new \Swift_Plugins_Loggers_ArrayLogger();
        $mailer->registerPlugin(new \Swift_Plugins_LoggerPlugin($logger));

        $body = $this->renderView(
            'email/contactoDesdeApp.html.twig',
            [
                'mensaje' => $mensaje,
                'celular' => $cliente->getMobile(),
                'nombre' => $cliente->getName(),
            ]
        );

        $message = (new \Swift_Message('Contacto desde App Qr'))
            ->setFrom('contactoapp@bodegalostoneles.com')
            ->setTo('turismo@bodegalostoneles.com')
            ->setBody($body, 'text/html');


        if (!$mailer->send($message)) {

            echo "Error:" . $logger->dump();
        } else {
            echo "Successfull.";
        }


        return new JsonResponse("Ok");
    }


}

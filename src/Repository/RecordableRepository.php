<?php

namespace App\Repository;

use App\Entity\Recordable;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Recordable|null find($id, $lockMode = null, $lockVersion = null)
 * @method Recordable|null findOneBy(array $criteria, array $orderBy = null)
 * @method Recordable[]    findAll()
 * @method Recordable[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RecordableRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Recordable::class);
    }

    // /**
    //  * @return Recordable[] Returns an array of Recordable objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Recordable
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PacienteRepository")
 * @UniqueEntity("idNumber")
 */
class Paciente
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $age;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $healthServiceNumber;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $healthCare;

    /**
     * @ORM\Column(type="string", length=255, nullable=true, unique=true)
     */
    private $idNumber;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $phoneNumber;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $celphoneNumber;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $contactPhoneNumber;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $currentIlnesses;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $currentMedication;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $alergies;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $epidemiologyNexus;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $epidemiologyNexusDescription;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $vaccines;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $toxicHabits;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $surgicalProdedures;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $gycologicalHistory;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $lastName;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $medicalMotive;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $bloodType;



    public function getId()
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getAge(): ?int
    {
        return $this->age;
    }

    public function setAge(?int $age): self
    {
        $this->age = $age;

        return $this;
    }

    public function getHealthServiceNumber(): ?string
    {
        return $this->healthServiceNumber;
    }

    public function setHealthServiceNumber(?string $healthServiceNumber): self
    {
        $this->healthServiceNumber = $healthServiceNumber;

        return $this;
    }

    public function getHealthCare(): ?string
    {
        return $this->healthCare;
    }

    public function setHealthCare(?string $healthCare): self
    {
        $this->healthCare = $healthCare;

        return $this;
    }

    public function getIdNumber(): ?string
    {
        return $this->idNumber;
    }

    public function setIdNumber(?string $idNumber): self
    {
        $this->idNumber = $idNumber;

        return $this;
    }


    public function getCurrentIlnesses(): ?string
    {
        return $this->currentIlnesses;
    }

    public function setCurrentIlnesses(?string $currentIlnesses): self
    {
        $this->currentIlnesses = $currentIlnesses;

        return $this;
    }

    public function getCurrentMedication(): ?string
    {
        return $this->currentMedication;
    }

    public function setCurrentMedication(?string $currentMedication): self
    {
        $this->currentMedication = $currentMedication;

        return $this;
    }

    public function getAlergies(): ?string
    {
        return $this->alergies;
    }

    public function setAlergies(?string $alergies): self
    {
        $this->alergies = $alergies;

        return $this;
    }

    public function getEpidemiologyNexus(): ?bool
    {
        return $this->epidemiologyNexus;
    }

    public function setEpidemiologyNexus(?bool $epidemiologyNexus): self
    {
        $this->epidemiologyNexus = $epidemiologyNexus;

        return $this;
    }

    public function getEpidemiologyNexusDescription(): ?string
    {
        return $this->epidemiologyNexusDescription;
    }

    public function setEpidemiologyNexusDescription(?string $epidemiologyNexusDescription): self
    {
        $this->epidemiologyNexusDescription = $epidemiologyNexusDescription;

        return $this;
    }

    public function getVaccines(): ?string
    {
        return $this->vaccines;
    }

    public function setVaccines(?string $vaccines): self
    {
        $this->vaccines = $vaccines;

        return $this;
    }

    public function getToxicHabits(): ?string
    {
        return $this->toxicHabits;
    }

    public function setToxicHabits(?string $toxicHabits): self
    {
        $this->toxicHabits = $toxicHabits;

        return $this;
    }

    public function getSurgicalProdedures(): ?string
    {
        return $this->surgicalProdedures;
    }

    public function setSurgicalProdedures(?string $surgicalProdedures): self
    {
        $this->surgicalProdedures = $surgicalProdedures;

        return $this;
    }

    public function getGycologicalHistory(): ?string
    {
        return $this->gycologicalHistory;
    }

    public function setGycologicalHistory(?string $gycologicalHistory): self
    {
        $this->gycologicalHistory = $gycologicalHistory;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getMedicalMotive(): ?string
    {
        return $this->medicalMotive;
    }

    public function setMedicalMotive(?string $medicalMotive): self
    {
        $this->medicalMotive = $medicalMotive;

        return $this;
    }

    public function getPhoneNumber(): ?string
    {
        return $this->phoneNumber;
    }

    public function setPhoneNumber(?string $phoneNumber): self
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    public function getCelphoneNumber(): ?string
    {
        return $this->celphoneNumber;
    }

    public function setCelphoneNumber(?string $celphoneNumber): self
    {
        $this->celphoneNumber = $celphoneNumber;

        return $this;
    }

    public function getContactPhoneNumber(): ?string
    {
        return $this->contactPhoneNumber;
    }

    public function setContactPhoneNumber(?string $contactPhoneNumber): self
    {
        $this->contactPhoneNumber = $contactPhoneNumber;

        return $this;
    }

    public function getBloodType(): ?string
    {
        return $this->bloodType;
    }

    public function setBloodType(?string $bloodType): self
    {
        $this->bloodType = $bloodType;

        return $this;
    }

    public function getInformedConsent(): ?bool
    {
        return $this->informedConsent;
    }

    public function setInformedConsent(bool $informedConsent): self
    {
        $this->informedConsent = $informedConsent;

        return $this;
    }
}

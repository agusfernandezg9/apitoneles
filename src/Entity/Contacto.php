<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass="App\Repository\ContactoRepository")
 */
class Contacto
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $direccion;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $telefono;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $facebook;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $instagram;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $link_tienda;


    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $link_reservas;


    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $link_carta;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $link_catalogo;


    /**
     * @return mixed
     */
    public function getLinkTienda()
    {
        return $this->link_tienda;
    }

    /**
     * @param mixed $link_tienda
     */
    public function setLinkTienda($link_tienda): void
    {
        $this->link_tienda = $link_tienda;
    }

    /**
     * @return mixed
     */
    public function getLinkReservas()
    {
        return $this->link_reservas;
    }

    /**
     * @param mixed $link_reservas
     */
    public function setLinkReservas($link_reservas): void
    {
        $this->link_reservas = $link_reservas;
    }

    /**
     * @return mixed
     */
    public function getLinkCarta()
    {
        return $this->link_carta;
    }

    /**
     * @param mixed $link_carta
     */
    public function setLinkCarta($link_carta): void
    {
        $this->link_carta = $link_carta;
    }



    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDireccion(): ?string
    {
        return $this->direccion;
    }

    public function setDireccion(string $direccion): self
    {
        $this->direccion = $direccion;

        return $this;
    }

    public function getTelefono(): ?string
    {
        return $this->telefono;
    }

    public function setTelefono(string $telefono): self
    {
        $this->telefono = $telefono;

        return $this;
    }

    public function getFacebook(): ?string
    {
        return $this->facebook;
    }

    public function setFacebook(?string $facebook): self
    {
        $this->facebook = $facebook;

        return $this;
    }

    public function getInstagram(): ?string
    {
        return $this->instagram;
    }

    public function setInstagram(?string $instagram): self
    {
        $this->instagram = $instagram;

        return $this;
    }

    public function getLinkCatalogo(): ?string
    {
        return $this->link_catalogo;
    }

    public function setLinkCatalogo(?string $link_catalogo): self
    {
        $this->link_catalogo = $link_catalogo;

        return $this;
    }


}

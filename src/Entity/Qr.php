<?php

namespace App\Entity;


use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity(repositoryClass="App\Repository\QrRepository")
 */
class Qr implements Recordable
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $codigo;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Client", inversedBy="qrs")
     */
    private $client;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Descuento", inversedBy="qrs")
     * @ORM\JoinColumn(nullable=false)
     */
    private $descuento;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $useDate;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $used;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $fechaCreacion;


    public function __toString()
    {
        return $this->getCodigo();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCodigo(): ?string
    {
        return $this->codigo;
    }

    public function setCodigo(string $codigo): self
    {
        $this->codigo = $codigo;

        return $this;
    }

    public function getClient(): ?Client
    {
        return $this->client;
    }

    public function setClient(?Client $client): self
    {
        $this->client = $client;

        return $this;
    }

    public function getDescuento(): ?Descuento
    {
        return $this->descuento;
    }

    public function setDescuento(?Descuento $descuento): self
    {
        $this->descuento = $descuento;

        return $this;
    }

    public function getUseDate(): ?\DateTimeInterface
    {
        return $this->useDate;
    }

    public function setUseDate(?\DateTimeInterface $useDate): self
    {
        $this->useDate = $useDate;

        return $this;
    }

    public function getUsed(): ?bool
    {
        return $this->used;
    }

    public function setUsed(?bool $used): self
    {
        $this->used = $used;

        return $this;
    }


    public function getFechaCreacion()
    {
        return $this->fechaCreacion;
    }

    public function setFechaCreacion(?\DateTimeInterface $fechaCreacion)
    {
        $this->fechaCreacion = $fechaCreacion;

        return $this;
    }
}

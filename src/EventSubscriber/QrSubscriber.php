<?php

namespace App\EventSubscriber;

use ApiPlatform\Core\EventListener\EventPriorities;

use App\Entity\Qr;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent;
use Symfony\Component\HttpKernel\KernelEvents;


class QrSubscriber implements EventSubscriberInterface
{

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::VIEW => ['qrGetPostRequest', EventPriorities::PRE_WRITE],
        ];
    }

    public function qrGetPostRequest(GetResponseForControllerResultEvent $event)
    {
        $qr = $event->getControllerResult();
        $method = $event->getRequest()->getMethod();

        if (!in_array($method, [Request::METHOD_POST, Request::METHOD_PUT])) {
            return;
        }

        switch ($method) {
            case(Request::METHOD_POST):
                if ($qr instanceof Qr) {
                    $uniqid = uniqid();
                    $rand_start = rand(1, 5);
                    $rand_8_char = substr($uniqid, $rand_start, 8);

                    $qr->setCodigo(strtoupper($rand_8_char));
                }
                break;
            case(Request::METHOD_PUT):
                if ($qr instanceof Qr) {
                    if ($qr->getUsed() == false || $qr->getUsed() == null) {
                        $now = new \DateTime(date('Y-m-d H:i:s', time()));
                        $qr->setUseDate($now);
                        $qr->setUsed(true);
                    }
                }
                break;
        }
    }

}

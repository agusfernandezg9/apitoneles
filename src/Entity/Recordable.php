<?php

namespace App\Entity;



interface Recordable
{
    public function getFechaCreacion();
    public function setFechaCreacion(\DateTimeInterface $fechaCreacion);
}

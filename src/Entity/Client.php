<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ClientRepository")
 */
class Client
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $mobile;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Qr", mappedBy="client")
     */
    private $qrs;

    /**
     * @ORM\Column(type="string", length=15, nullable=true)
     */
    private $role;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $expo_push_token;

    /**
     * @return mixed
     */
    public function getExpoPushToken()
    {
        return $this->expo_push_token;
    }

    /**
     * @param mixed $expo_push_token
     */
    public function setExpoPushToken($expo_push_token): void
    {
        $this->expo_push_token = $expo_push_token;
    }

    public function __construct()
    {
        $this->qrs = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->getName();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getMobile(): ?string
    {
        return $this->mobile;
    }

    public function setMobile(string $mobile): self
    {
        $this->mobile = $mobile;

        return $this;
    }

    /**
     * @return Collection|Qr[]
     */
    public function getQrs(): Collection
    {
        return $this->qrs;
    }

    public function addQr(Qr $qr): self
    {
        if (!$this->qrs->contains($qr)) {
            $this->qrs[] = $qr;
            $qr->setClient($this);
        }

        return $this;
    }

    public function removeQr(Qr $qr): self
    {
        if ($this->qrs->contains($qr)) {
            $this->qrs->removeElement($qr);
            // set the owning side to null (unless already changed)
            if ($qr->getClient() === $this) {
                $qr->setClient(null);
            }
        }

        return $this;
    }

    public function getRole(): ?string
    {
        return $this->role;
    }

    public function setRole(?string $role): self
    {
        $this->role = $role;

        return $this;
    }
}

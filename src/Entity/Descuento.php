<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @Vich\Uploadable
 * @ORM\Entity(repositoryClass="App\Repository\DescuentoRepository")
 */
class Descuento
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    private $titulo;

    /**
     * @ORM\Column(type="text", nullable=false)
     */
    private $descripcion;

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    private $porcentaje;

    /**
     * @ORM\Column(type="boolean", options={"default" : "1"},nullable=true)
     */
    private $activo;


    /**
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $image;

    /**
     * @Vich\UploadableField(mapping="images_desc", fileNameProperty="url")
     * @var File
     */
    private $imageFile;

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Qr", mappedBy="descuento")
     */
    private $qrs;

    /**
     * @ORM\Column(type="date",nullable=false)
     */
    private $validez;


    public function __construct()
    {
        $this->qrs = new ArrayCollection();
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt(): \DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $updatedAt
     */
    public function setUpdatedAt(\DateTime $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }


    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;

        if ($image) {

            $this->updatedAt = new \DateTime('now');
        }
    }

    public function getImageFile()
    {
        return $this->imageFile;
    }

    public function setUrl($image)
    {
        $this->image = $image;
    }

    public function getImage()
    {
        return '/imagesdesc/' . $this->image;
    }

    public function getUrl()
    {
        return $this->image;
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitulo(): ?string
    {
        return $this->titulo;
    }

    public function setTitulo(?string $titulo): self
    {
        $this->titulo = $titulo;

        return $this;
    }

    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    public function setDescripcion(?string $descripcion): self
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    public function getPorcentaje(): ?int
    {
        return $this->porcentaje;
    }

    public function setPorcentaje(?int $porcentaje): self
    {
        $this->porcentaje = $porcentaje;

        return $this;
    }

    public function getActivo(): ?bool
    {
        return $this->activo;
    }

    public function setActivo(bool $activo = true): self
    {
        $this->activo = $activo;

        return $this;
    }

    /**
     * @return Collection|Qr[]
     */
    public function getQrs(): Collection
    {
        return $this->qrs;
    }

    public function addQr(Qr $qr): self
    {
        if (!$this->qrs->contains($qr)) {
            $this->qrs[] = $qr;
            $qr->setDescuento($this);
        }

        return $this;
    }

    public function removeQr(Qr $qr): self
    {
        if ($this->qrs->contains($qr)) {
            $this->qrs->removeElement($qr);
            // set the owning side to null (unless already changed)
            if ($qr->getDescuento() === $this) {
                $qr->setDescuento(null);
            }
        }
        return $this;
    }

    public function getValidez(): ?\DateTimeInterface
    {
        return $this->validez;
    }


    public function setValidez(\DateTimeInterface $validez): self
    {
        $this->validez = $validez;

        return $this;
    }


}

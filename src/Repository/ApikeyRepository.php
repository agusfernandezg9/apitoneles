<?php

namespace App\Repository;

use App\Entity\Apikey;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Apikey|null find($id, $lockMode = null, $lockVersion = null)
 * @method Apikey|null findOneBy(array $criteria, array $orderBy = null)
 * @method Apikey[]    findAll()
 * @method Apikey[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ApikeyRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Apikey::class);
    }

    // /**
    //  * @return Apikey[] Returns an array of Apikey objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Apikey
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

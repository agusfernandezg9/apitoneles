<?php


namespace App\Controller;



use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;

class TokenGenerator extends AbstractController
{

    /**
     * @Route("/tokenGenerate", name="token_generate")
     */
    public function tokenGenerate( JWTTokenManagerInterface $JWTManager)
    {
        $request = $this->get('request_stack')->getCurrentRequest();
        $id_user = $request->get('userid');

        $em = $this->getDoctrine()->getManager();

        $user = $em->getRepository('App\Entity\User')->find($id_user);

        return new JsonResponse(['token' => $JWTManager->create($user)]);
    }

}
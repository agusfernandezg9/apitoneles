<?php

namespace App\Controller;

use App\Entity\Client;

use ExponentPhpSDK\Expo;
use ExpoSDK\ExpoMessage;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class PushNotificationController extends AbstractController
{
    /**
     * @Route("/push/notification", name="push_notification")
     */
    public function index(Request $request): Response
    {
        $repository = $this->getDoctrine()->getRepository(Client::class);
        $clients = $repository->findAll();
        $expoTokens = [];
        $message = '';

        foreach ($clients as $client) {
            $expoToken = $client->getExpoPushToken() ? $client->getExpoPushToken() : null;
            if ($expoToken && $expoToken != 'no_dio_los_permisos') {
                array_push($expoTokens, $expoToken);
            }
        }

         $tokens_sin_repetir = array_unique($expoTokens);
        //$tokens_sin_repetir = ['ExponentPushToken[ZJFL5SE0ZGHn5QzWdjw7UM]'];

        $array_tokens = array_slice($tokens_sin_repetir, 10);

        $defaultData = ['message' => 'Ingrese el título y mensaje de al notificación'];
        $form = $this->createFormBuilder($defaultData)
            ->add('message_title', TextType::class, [
                'label' => 'Título',
                'attr' => [
                    'class' => 'form-control form-group',
                ],
                'constraints' => [
                    new NotBlank(),
                    new Length(['min' => 4]),
                ],
            ])
            ->add('message_body', TextareaType::class, [
                'label' => 'Contenido',
                'attr' => [
                    'class' => 'form-control form-group',
                ],
                'constraints' => [
                    new NotBlank(),
                ],
            ])
            ->add('Enviar', SubmitType::class, [
                'attr' => [
                    'class' => 'form-control form-group',
                ],
            ])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();

            if ($tokens_sin_repetir) {
                try {
//                    $channelName = 'qrs';
//                    $expo = Expo::normalSetup();
//
//                    foreach ($tokens_sin_repetir as $token) {
//                        $expo->subscribe($channelName, $token);
//                    }
//
//                    $notification = [
//                        "badge" => 1,
//                        'title' => $data['message_title'],
//                        'body' => $data['message_body'],
//                    ];
//
//
//                    // Notify an interest with a notification
//                    $expo->notify([$channelName], $notification);

                    $expo = \ExpoSDK\Expo::driver('file');
                    $message = (new ExpoMessage())
                        ->setTitle($data['message_title'])
                        ->setBody($data['message_body'])
                        ->setChannelId('default')
                        ->setBadge(0)
                        ->playSound();

                    foreach ($array_tokens as $tokens) {

                        $channel = 'qrs';

                        $expo->subscribe($channel, $tokens);

                        $response = $expo->send($message)->to($tokens)->push();

                        $data = $response->getData();
                        //print_r($data);
                    }


                    $message = ['status' => 'success', 'text' => 'Se enviaron las notificaciones a ' . count($tokens_sin_repetir) . ' usuarios.'];
                } catch (\Exception $e) {

                    print_r($e->getMessage());
                }

            } else {
                $message = ['status' => 'warning', 'text' => 'No hay clientes que hayan autorizado el envío de notitificaciones, no se enviaron notificaciones push.'];
            }

        }

        return $this->render('push_notification/index.html.twig', [
            'form' => $form->createView(),
            'message' => $message
        ]);
    }
}
